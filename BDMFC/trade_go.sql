-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 14-12-2018 a las 16:52:12
-- Versión del servidor: 10.1.36-MariaDB
-- Versión de PHP: 5.6.38

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `trade&go`
--

--
-- Volcado de datos para la tabla `citas`
--

INSERT INTO `citas` (`idcita`, `horacita`, `diacita`, `asuntocita`, `telefono`, `observaciones`) VALUES
(53, '10:44:00', '2018-12-13', 'mirar si el objeto no es una copia', 665456789, 'hablar con Antonio para ver si la compra no es falsa'),
(54, '10:45:00', '2018-12-13', 'mirar si el objeto no es una copia', 665456789, 'hablar con Antonio para ver si la compra no es falsa');

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`id`, `nombre`, `apellidos`, `correo`, `usuario`, `clave`, `telefono`) VALUES
(8, 'Antonio', 'Perez Gomez', 'AntonioGomez@gmail.com', 'AntonioGo', 'abc123.', 665456789),
(11, 'Antonio', 'Gomez Suarez', 'AntonioGomez2@gmail.com', 'AntonioGo2', '1232131', 678765678);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
